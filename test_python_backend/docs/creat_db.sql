BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS `user_site` (
	`id`	INTEGER PRIMARY KEY AUTOINCREMENT,
	`country`	TEXT,
	`country_name`	TEXT,
	`ip_addr`	TEXT
);
CREATE TABLE IF NOT EXISTS `user_action` (
	`id`	INTEGER PRIMARY KEY AUTOINCREMENT,
	`date_action`	TEXT,
	`code_action`	TEXT,
	`user`	INTEGER,
	`cart_id`	INTEGER,
	`action`	INTEGER,
	`note_action`	TEXT,
	FOREIGN KEY(`user`) REFERENCES `user_site`(`id`),
	FOREIGN KEY(`action`) REFERENCES `site_action`(`id`)
);
CREATE TABLE IF NOT EXISTS `site_action` (
	`id`	INTEGER PRIMARY KEY AUTOINCREMENT,
	`action`	TEXT,
	`note`	TEXT
);
INSERT INTO `site_action` VALUES (1,'home',NULL);
INSERT INTO `site_action` VALUES (2,'next_fresh_fish',NULL);
INSERT INTO `site_action` VALUES (3,'next_canned_food',NULL);
INSERT INTO `site_action` VALUES (4,'next_caviar',NULL);
INSERT INTO `site_action` VALUES (5,'next_semi_manufactures',NULL);
INSERT INTO `site_action` VALUES (6,'next_frozen_fish',NULL);
INSERT INTO `site_action` VALUES (7,'goods_in_basket',NULL);
INSERT INTO `site_action` VALUES (8,'success_pay',NULL);
INSERT INTO `site_action` VALUES (9,'pay_basket',NULL);
COMMIT;

