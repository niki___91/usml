#!/usr/bin/python3
# -*- coding: UTF-8 -*-

"""
    Сбор статистики по данным из базы данных.
"""

import sqlite3


def main():
    def select_1():
        """
        Посетители из какой страны чаще всего посещают сайт.
        """
        cursor.execute("""SELECT `country_name`, count(`country`) AS `c` FROM `user_site`
                            WHERE `country` != '-'
                            GROUP BY `country` ORDER BY `c` DESC LIMIT 1""")
        return cursor.fetchone()

    def select_2(row_count=1):
        """
        Посетители из какой страны чаще всего интересуются товарами из определенной категории “fresh_fish”.
        """
        cursor.execute("""SELECT `country_name`, COUNT(`id`) AS `count_id` FROM (SELECT * FROM `user_action` `ua`
                                LEFT JOIN `user_site` `us` ON `us`.`id` = `ua`.`user`
                                WHERE `ua`.`note_action` LIKE '%/fresh_fish/%' AND `us`.`country` != '-'
                                GROUP BY `ua`.`user`)
                            GROUP BY `country_name`
                            ORDER BY `count_id` DESC LIMIT ?""", (row_count, ))
        return cursor.fetchall()

    def select_3():
        """
        В какое время суток чаще всего просматривают категорию “frozen_fish”
        Ночь (00:00 - 06:00)
        Утро (06:00 - 12:00)
        День (12:00 - 18:00)
        Вечер (18:00 - 0:00)
        """
        time_day = {'night': ['00', '05'], 'morning': ['06', '11'], 'day': ['12', '17'], 'evening': ['18', '23']}
        temp_count = [0, 0]

        for key, value in time_day.items():
            cursor.execute("""SELECT COUNT(`ua`.`id`) FROM `user_action` `ua`
                                LEFT JOIN `site_action` `sa` ON `sa`.`id` = `ua`.`action`
                                WHERE `sa`.`action` = 'next_frozen_fish' 
                                AND STRFTIME('%H', `ua`.`date_action`) BETWEEN ? AND ? """, (value[0], value[1]))
            x = cursor.fetchone()[0]
            if x > temp_count[1]:
                temp_count[0] = key
                temp_count[1] = x
        return temp_count[0]

    def select_4():
        """
        Какое среднее и медианное время пользователь проводит на сайте
        """
        temp_list = []

        cursor.execute("""SELECT 
                            ((JULIANDAY(MAX(`date_action`)) - JULIANDAY(MIN(`date_action`)))  * 86400) / 60 AS `minute` 
                            FROM `user_action` `ua`
                            GROUP BY `user`, DATE(`date_action`)""")
        # Делаем выборку одним списком
        for i in cursor.fetchall():
            temp_list.append(i[0])

        avg = int(sum(temp_list) / len(temp_list))
        if len(temp_list) % 2 == 0:
            median = int(sum((int(temp_list[int(len(temp_list) / 2)]), int(temp_list[int(len(temp_list) / 2) + 1]))))
        else:
            median = int(temp_list[int(len(temp_list) / 2)])
        return [avg, median]

    def select_5():
        """
        Сколько пользователей возвращаются повторно на сайт.
        """
        cursor.execute("""SELECT COUNT(`count_id`) FROM (SELECT `user`, COUNT(`id`) AS `count_id`
                            FROM (SELECT `ua`.`id`, `ua`.`user` FROM `user_action` `ua`
                                GROUP BY `ua`.`user`, DATE(`ua`.`date_action`))
                            GROUP BY `user`)
                        WHERE `count_id` >= 2""")
        return cursor.fetchone()[0]

    def select_6():
        """
        Сколько брошенных (не оплаченных) корзин имеется.
        """
        cursor.execute("""SELECT COUNT(DISTINCT(`ua`.`cart_id`)) FROM `user_action` `ua`
                            LEFT JOIN `site_action` `sa` ON `sa`.`id` = `ua`.`action`
                            WHERE `ua`.`cart_id` IS NOT NULL AND `sa`.`action` == 'goods_in_basket'
                            AND `ua`.`cart_id` NOT IN (SELECT `ua1`.`cart_id` FROM `user_action` `ua1`
                                LEFT JOIN `site_action` `sa1` ON `sa1`.`id` = `ua1`.`action`
                                WHERE `sa1`.`action` == 'success_pay')""")
        return cursor.fetchone()[0]

    def select_7():
        """
        Какая конверсия у сайта.
        """
        cursor.execute("""SELECT COUNT(`ua`.`id`), (SELECT COUNT(DISTINCT(`ua1`.`user`)) 
                                                FROM `user_action` `ua1`) FROM `user_action` `ua`
                            LEFT JOIN `site_action` `sa` ON `sa`.`id` = `ua`.`action`
                            WHERE `sa`.`action` == 'success_pay'""")
        temp_tuple = cursor.fetchone()
        return (temp_tuple[0] / temp_tuple[1]) * 100

    def select_8(row_count=1):
        """
        Посетители из какой страны совершают больше всего действий на сайте.
        """
        cursor.execute("""SELECT `us`.`country_name`, COUNT(`ua`.`id`) AS `action_count` FROM `user_action` `ua`
                            LEFT JOIN `user_site` `us` ON `us`.`id` = `ua`.`user`
                            WHERE `us`.`country_name` != '-'
                            GROUP BY `country` ORDER BY `action_count` DESC LIMIT ?""", (row_count, ))
        return cursor.fetchall()

    def select_9():
        """
        Какое максимальное число запросов на сайт за астрономический час (c 00 минут 00 секунд до 59 минут 59 секунд).
        """
        cursor.execute("""SELECT COUNT(`ua`.`id`) AS `action_count` FROM `user_action` `ua`
                            GROUP BY STRFTIME('%Y-%m-%d %H', `ua`.`date_action`)
                            ORDER BY `action_count` DESC LIMIT 1""")
        return cursor.fetchone()[0]

    def select_10():
        """
        Товары из какой категории чаще всего покупают совместно с товаром из категории “semi_manufactures”.
        """
        temp_rez = {}
        rez = {}

        cursor.execute("""SELECT `ua1`.`cart_id`, `sa`.`action`
                    FROM `user_action` `ua`
                    LEFT JOIN `site_action` `sa` ON `sa`.`id` = `ua`.`action`
                    LEFT JOIN `user_action` `ua1` ON `ua1`.`id` = (SELECT CASE WHEN `sa`.`action` = 'goods_in_basket' 
                                                                            THEN `ua2`.`id` END
                            FROM `user_action` `ua2` 
                            LEFT JOIN `site_action` `sa` ON `sa`.`id` = `ua2`.`action`
                            WHERE `ua2`.`user` = `ua`.`user` AND DATE(`ua2`.`date_action`) = DATE(`ua`.`date_action`) 
                            AND `ua2`.`id` > `ua`.`id` LIMIT 1)
                    WHERE `ua`.`note_action` LIKE 'https://all_to_the_bottom.com/%/%' AND `ua1`.`id` IS NOT NULL 
                    AND EXISTS (SELECT * FROM `user_action` `ua3` 
                                    LEFT JOIN `site_action` `sa` ON `sa`.`id` = `ua3`.`action` 
                                    WHERE `sa`.`action` = 'success_pay' AND `ua1`.`cart_id` = `ua3`.`cart_id`)
                    """)
        temp_list = cursor.fetchall()

        for i in temp_list:
            if temp_rez.get(i[0]):
                temp_rez[i[0]].append(i[1])
            else:
                temp_rez[i[0]] = [i[1]]
        else:
            for key, value in temp_rez.items():
                if len(value) > 1:
                    for x in value:
                        if x == 'next_semi_manufactures':
                            for y in value:
                                if y != 'next_semi_manufactures':
                                    if rez.get(y):
                                        rez[y] += 1
                                    else:
                                        rez[y] = 1
        return {x: y for x, y in filter(lambda x: rez[x[0]] == max(rez.values()), rez.items())}

    def select_11():
        """
        Какое количество пользователей совершали повторные покупки.
        """
        cursor.execute("""SELECT COUNT(`count_id`) FROM (SELECT `user`, COUNT(`id`) AS `count_id`
                            FROM (SELECT `ua`.`id`, `ua`.`user` FROM `user_action` `ua`
                                LEFT JOIN `site_action` `sa` ON `sa`.`id` = `ua`.`action`
                                WHERE `sa`.`action` = 'success_pay')
                            GROUP BY `user`)
                          WHERE `count_id` >= 2""")
        return cursor.fetchone()[0]

    # Подключение к DB
    connect_sqlite = sqlite3.connect("./parser_log.db")
    cursor = connect_sqlite.cursor()

    print('Результат работы генератора отчетов:')
    print('Посетители из какой страны чаще всего посещают сайт:                     {} = {}'.
          format(select_1()[0], select_1()[1]))
    print('Посетители из какой страны чаще всего интересуются товарами из определенной категории fresh_fish:')
    for i in select_2(6):
        print('{} = {}'.format(i[0], i[1]))
    print('В какое время суток чаще всего просматривают категорию “frozen_fish”:    {}'.
          format(select_3()))
    print('Какое среднее и медианное время пользователь проводит на сайте:          {} мин.| {} мин.'
          .format(select_4()[0], select_4()[1]))
    print('Сколько пользователей возвращаются повторно на сайт:                     {}'.format(select_5()))
    print('Сколько брошенных (не оплаченных) корзин имеется:                        {}'.format(select_6()))
    print('Какая конверсия у сайта:                                                 {}%'
          .format(round(select_7())))
    print('Посетители из какой страны совершают больше всего действий на сайте: ')
    for i in select_8(5):
        print('{} = {}'.format(i[0], i[1]))
    print('Какое максимальное число запросов на сайт за астрономический час: {}'.format(select_9()))
    print('Товары из какой категории чаще всего покупают совместно с товаром из категории “semi_manufactures”: {}'
          .format(select_10()))
    print('Какое количество пользователей совершали повторные покупки: {}'
          .format(select_11()))


if __name__ == '__main__':
    main()
