#!/usr/bin/python3
# -*- coding: UTF-8 -*-

"""
    Парсер анализирует данные с лог файла и перемещает с обработкой в базу данных для дальнейшего анализа.
"""

import sqlite3
import urllib.request
from urllib.parse import urlparse
import json
import re
import os


def get_country(ip_addr):
    """
    Функция возвращает по ip  адресу пользователя его страну.
    """
    print('Запрос данных с GeoIP:   {}'.format(ip_addr))
    urlopen = urllib.request.urlopen('http://ru.sxgeo.city/json/{}'.format(ip_addr))
    if urlopen.getcode() != 200:
        print('Не удалось получить данные об ip адресе, работа остановлена.')
        exit()
    else:
        temp_json = json.loads(urlopen.read())

    try:
        return [temp_json['country']['iso'], temp_json['country']['name_ru']]
    except KeyError:
        return ['-', '-']
    except TypeError:
        return ['-', '-']


def get_action(url):
    temp_url = urlparse(url)
    action = None
    cart_id = None

    if temp_url.path != "":
        if re.match(r'/fresh_fish', temp_url.path) is not None:
            action = 2
        elif re.match(r'/canned_food', temp_url.path) is not None:
            action = 3
        elif re.match(r'/caviar', temp_url.path) is not None:
            action = 4
        elif re.match(r'/semi_manufactures', temp_url.path) is not None:
            action = 5
        elif re.match(r'/frozen_fish', temp_url.path) is not None:
            action = 6
        elif temp_url.path == "/cart":
            action = 7
            cart_id = int(temp_url.query.split('&')[2].split('=')[1])
        elif re.match(r'/success_pay_', temp_url.path) is not None:
            action = 8
            cart_id = int(temp_url.path.split('_')[2])
        elif temp_url.path == "/pay":
            action = 9
            cart_id = int(temp_url.query.split('&')[1].split('=')[1])
        else:
            print('Не известное действие на сайте!')
            pass
    else:
        action = 1
    return [action, cart_id]


def main():
    # Метрики
    count_line = 0
    count_dupli = 0
    count_get_geoip = 0

    # Удаляем старую базу данных
    os.system('rm -f ./parser_log.db')

    # Подключение к DB
    connect_sqlite = sqlite3.connect("./parser_log.db")
    cursor = connect_sqlite.cursor()

    # Создаем нужные таблицы и данные
    cursor.executescript("""BEGIN TRANSACTION;
                        CREATE TABLE IF NOT EXISTS `user_site` (
                            `id`	INTEGER PRIMARY KEY AUTOINCREMENT,
                            `country`	TEXT,
                            `country_name`	TEXT,
                            `ip_addr`	TEXT
                        );
                        CREATE TABLE IF NOT EXISTS `user_action` (
                            `id`	INTEGER PRIMARY KEY AUTOINCREMENT,
                            `date_action`	TEXT,
                            `code_action`	TEXT,
                            `user`	INTEGER,
                            `cart_id`	INTEGER,
                            `action`	INTEGER,
                            `note_action`	TEXT,
                            FOREIGN KEY(`user`) REFERENCES `user_site`(`id`),
                            FOREIGN KEY(`action`) REFERENCES `site_action`(`id`)
                        );
                        CREATE TABLE IF NOT EXISTS `site_action` (
                            `id`	INTEGER PRIMARY KEY AUTOINCREMENT,
                            `action`	TEXT,
                            `note`	TEXT
                        );
                        INSERT INTO `site_action` VALUES (1,'home',NULL);
                        INSERT INTO `site_action` VALUES (2,'next_fresh_fish',NULL);
                        INSERT INTO `site_action` VALUES (3,'next_canned_food',NULL);
                        INSERT INTO `site_action` VALUES (4,'next_caviar',NULL);
                        INSERT INTO `site_action` VALUES (5,'next_semi_manufactures',NULL);
                        INSERT INTO `site_action` VALUES (6,'next_frozen_fish',NULL);
                        INSERT INTO `site_action` VALUES (7,'goods_in_basket',NULL);
                        INSERT INTO `site_action` VALUES (8,'success_pay',NULL);
                        INSERT INTO `site_action` VALUES (9,'pay_basket',NULL);
                        COMMIT;""")

    # Открываем файл для чтение
    read_file = open("./docs/logs.txt", 'r')

    # Перебираем все строки в лог файле
    for line in read_file:
        print(line)
        temp_list_geo = []
        temp_list_action = []
        temp_line = line.split(' ')

        # ip адрес
        temp_list_geo.append(temp_line[11])

        # Дата
        temp_list_action.append(' '.join((temp_line[7], temp_line[8])))

        # Код действия
        temp_list_action.append(temp_line[9][1:-1])

        # Действие на ресурсе
        temp_list_action.append(re.sub("/\\n$|\\n$", '', temp_line[12]))

        # Запрос: Есть ли запись в базе данных с этого ip адреса
        cursor.execute("""SELECT `id` FROM `user_site` WHERE `ip_addr` = ?;""", (temp_line[11], ))
        check_user_site = cursor.fetchone()

        # Проверка есть ли у нас такой пользователь в базе данных
        if check_user_site is None:
            # Страна
            temp_list_geo.extend(get_country(temp_line[11]))
            count_get_geoip += 1

            cursor.execute("""INSERT INTO `user_site` (`ip_addr`, `country`, `country_name`) 
                                VALUES (?, ?, ?);""", temp_list_geo)
            check_user_site = [cursor.lastrowid, ]

        temp_list_action.append(check_user_site[0])

        # Запрос: Есть ли строка с действиями в базе данных.
        cursor.execute("""SELECT `code_action` FROM `user_action` WHERE `code_action` = ?;""", (temp_list_action[1],))
        check_code_action = cursor.fetchone()

        # Тип действия
        temp_list_action.extend(get_action(temp_list_action[2]))

        # Если записи нет, то выносим данные в базу данных
        if check_code_action is None:
            cursor.execute("""INSERT INTO `user_action` (`date_action`, `code_action`, 
                                            `note_action`, `user`, `action`, `cart_id`) 
                                VALUES (?, ?, ?, ?, ?, ?);""", temp_list_action)
        else:
            count_dupli += 1

        print(temp_list_geo)
        print(temp_list_action)
        count_line += 1
    else:
        # Фиксируем изменения
        connect_sqlite.commit()

        # Запрос: Кол-во пользователей
        cursor.execute("""SELECT COUNT(`id`) FROM `user_site`;""")
        count_users = cursor.fetchone()

        # По окончанию выводим метрики
        print('')
        print('Результат работы парсера:')
        print('Кол-во пользователей:    {}'.format(count_users[0]))
        print('Кол-во строк:            {}'.format(count_line))
        print('Кол-во дубликатов:       {}'.format(count_dupli))
        print('Кол-во запросов в geoip: {}'.format(count_get_geoip))


if __name__ == '__main__':
    main()
