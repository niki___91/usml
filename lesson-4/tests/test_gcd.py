import unittest

from gcd import gcd


class GreatestCommonDiv(unittest.TestCase):
    def test_correct_data(self):
        test_data = ((0, 1, 1), (1, 0, 1), (10, 10, 10), (1, 1, 1), (12, 36, 12), (18, 60, 6),
                     (50, 75, 25), (111, 432, 3), (11, 18, 1), (661, 113, 1))
        for x in test_data:
            with self.subTest(data=x):
                self.assertEqual(gcd(x[0], x[1]), x[2])

    def test_incorrect_data(self):
        test_data = ((1.0, 1.3, 0), (1, 0, 0), (10, 10, 0), (1, 1, 0), (12, 36, 72))
        for x in test_data:
            with self.subTest(data=x):
                self.assertNotEqual(gcd(x[0], x[1]), x[2])
