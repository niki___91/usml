#!/usr/bin/env python

from random import randint


class Car:
    """
    Машина
    """
    CAR_SPECS = {
        'ferrary': {"max_speed": 340, "drag_coef": 0.324, "time_to_max": 26},
        'bugatti': {"max_speed": 407, "drag_coef": 0.39, "time_to_max": 32},
        'toyota': {"max_speed": 180, "drag_coef": 0.25, "time_to_max": 40},
        'lada': {"max_speed": 180, "drag_coef": 0.32, "time_to_max": 56},
        'sx4': {"max_speed": 180, "drag_coef": 0.33, "time_to_max": 44},
}

    def car_speed(self, competitor_name, competitor_time):
        _car = self.CAR_SPECS[competitor_name]
        return (competitor_time / _car["time_to_max"]) * _car['max_speed']


class Weather:
    """
    Погода
    """
    def __init__(self, wind_speed):
        self._wind_speed = wind_speed

    @property
    def wind_speed(self):
        return randint(0, self._wind_speed)


class Competition:
    """
    Соревнование
    """
    _exist_instance = None
    _competitor_time = 0

    def __new__(cls, *args, **kwargs):
        if cls._exist_instance is None:
            cls._exist_instance = super().__new__(cls)
        return cls._exist_instance

    def __init__(self, distance=0):
        self.car = Car()
        self._distance = distance
        self._competitor = ''
        self._register = []

    def start(self, competitor, wind_speed):
        _weather = Weather(wind_speed)
        for distance in range(self._distance):
            _wind_speed = _weather.wind_speed

            if self._competitor_time == 0:
                _speed = 1
            else:
                _speed = self.car.car_speed(competitor, self._competitor_time)
                if _speed > _wind_speed:
                    _speed -= (self.car.CAR_SPECS[competitor]["drag_coef"] * _wind_speed)

            self._competitor_time += float(1) / _speed
        self._competitor = competitor
        self._register.append((competitor, self._competitor_time))

    def end(self, *args, **kwargs):
        print("Car <%s> result: %f" % (self._register[-1][0], self._register[-1][1]))


class Race:
    def __init__(self, stages=None):
        self.__stages = list()
        self.__stages.extend(stages)

    def start(self, competitors):
        for competitor in competitors:
            for stage in self.__stages:
                stage(competitor, wind_speed=20)


competition = Competition(distance=10000)
competitors = ('ferrary', 'bugatti', 'toyota', 'lada', 'sx4')
race = Race([competition.start, competition.end])
race.start(competitors)
