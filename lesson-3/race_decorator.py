#!/usr/bin/env python

from random import randint
from abc import ABC, abstractmethod

CAR_SPECS = {
        'ferrary': {"max_speed": 340, "drag_coef": 0.324, "time_to_max": 26},
        'bugatti': {"max_speed": 407, "drag_coef": 0.39, "time_to_max": 32},
        'toyota': {"max_speed": 180, "drag_coef": 0.25, "time_to_max": 40},
        'lada': {"max_speed": 180, "drag_coef": 0.32, "time_to_max": 56},
        'sx4': {"max_speed": 180, "drag_coef": 0.33, "time_to_max": 44},
}


class AbstractWeather(ABC):
    @abstractmethod
    def wind_speed(self):
        pass

    @abstractmethod
    def air_humidity(self):
        pass

    @abstractmethod
    def air_temperature(self):
        pass


class WeatherDecorator(AbstractWeather):
    def __init__(self, base):
        self.base = base

    def wind_speed(self):
        self.base.wind_speed()

    def air_humidity(self):
        self.base.air_humidity()

    def air_temperature(self):
        self.base.air_temperature()


class Car:
    """
    Машина
    """
    def __init__(self, car_specs):
        self.specs = car_specs

    def speed(self, competitor_name, competitor_time):
        _car = self.specs[competitor_name]
        return (competitor_time / _car["time_to_max"]) * _car['max_speed']


class Weather(AbstractWeather):
    """
    Погода
    """
    def wind_speed(self):
        return randint(0, 2)

    def air_humidity(self):
        return randint(30, 60)

    def air_temperature(self):
        return randint(25, 35)


class Rain(WeatherDecorator):
    """
    Дождь
    """
    def air_humidity(self):
        return randint(80, 100)


class Wind(WeatherDecorator):
    """
    Ветер
    """
    def wind_speed(self):
        return randint(1, 20)


class Competition:
    """
    Соревнование
    """
    _exist_instance = None
    _competitor_time = 0

    def __new__(cls, *args, **kwargs):
        if cls._exist_instance is None:
            cls._exist_instance = super().__new__(cls)
        return cls._exist_instance

    def __init__(self, transport, distance=0):
        self.transport = transport
        self._distance = distance

    def start(self, competitors, weather):
        for _competitor_name in competitors:
            for distance in range(self._distance):
                _wind_speed = weather.wind_speed()

                if self._competitor_time == 0:
                    _speed = 1
                else:
                    _speed = self.transport.speed(_competitor_name, self._competitor_time)
                    if _speed > _wind_speed:
                        _speed -= (self.transport.specs[_competitor_name]["drag_coef"] *
                                   (_wind_speed + weather.air_humidity() + weather.air_temperature()))

                self._competitor_time += float(1) / _speed

            self.end(_competitor_name)

    def end(self, competitor_name):
        print("Car <%s> result: %f" % (competitor_name, self._competitor_time))


car = Car(CAR_SPECS)
weather = Weather()
competition_weather = Rain(weather)

competition = Competition(car, distance=10000)
competitors = ('ferrary', 'bugatti', 'toyota', 'lada', 'sx4')

competition.start(competitors, weather)
