#!/usr/bin/env python

from random import randint
from abc import ABC, abstractmethod

CAR_SPECS = {
        'ferrary': {"max_speed": 340, "drag_coef": 0.324, "time_to_max": 26,
                    "mass": 800, "width": 1.5, "height": 1.2, "length": 2.8},
        'bugatti': {"max_speed": 407, "drag_coef": 0.39, "time_to_max": 32,
                    "mass": 700, "width": 1.3, "height": 1.3, "length": 2.1},
        'toyota': {"max_speed": 180, "drag_coef": 0.25, "time_to_max": 40,
                   "mass": 600, "width": 1.4, "height": 1.2, "length": 2.2},
        'lada': {"max_speed": 180, "drag_coef": 0.32, "time_to_max": 56,
                 "mass": 1000, "width": 1.1, "height": 1.5, "length": 2.1},
        'sx4': {"max_speed": 180, "drag_coef": 0.33, "time_to_max": 44,
                "mass": 900, "width": 1.3, "height": 1.1, "length": 2},
}

MOTO_SPECS = {
        'ducati': {"max_speed": 300, "drag_coef": 0.501, "time_to_max": 5,
                   "mass": 200, "width": 0.3, "height": 0.5, "length": 1.5},
        'bmw': {"max_speed": 290, "drag_coef": 0.522, "time_to_max": 5,
                "mass": 100, "width": 0.4, "height": 0.5, "length": 1.4},
}


class AbstractTransport(ABC):
    @abstractmethod
    def speed(self, *args, **kwargs):
        pass

    @abstractmethod
    def mass(self, *args, **kwargs):
        pass

    @abstractmethod
    def width(self, *args, **kwargs):
        pass

    @abstractmethod
    def height(self, *args, **kwargs):
        pass

    @abstractmethod
    def length(self, *args, **kwargs):
        pass


class Transport(AbstractTransport):
    def __init__(self, specs):
        self._specs = specs
        self.competitor_name = None

    def speed(self):
        pass

    def mass(self):
        return self._specs[self.competitor_name].get('weight', 0)

    def width(self):
        return self._specs[self.competitor_name].get('weight', 0)

    def height(self):
        return self._specs[self.competitor_name].get('weight', 0)

    def length(self):
        return self._specs[self.competitor_name].get('length', 0)


class Car(Transport):
    """
    Машина
    """
    def _speed(self, competitor_time):
        _car = self._specs[self.competitor_name]
        return (competitor_time / _car["time_to_max"]) * _car['max_speed']

    def speed(self, competitor_time, wind_speed):
        if competitor_time == 0:
            __speed = 1
        else:
            __speed = self._speed(competitor_time)
            if __speed > wind_speed:
                __speed -= (self._specs[self.competitor_name]["drag_coef"] * wind_speed)
        return __speed


class Motorcycle(Transport):
    """
    Мотоцикл
    """
    def _speed(self, competitor_time):
        _moto = self._specs[self.competitor_name]
        return (competitor_time / _moto["time_to_max"]) * _moto['max_speed']

    def speed(self, competitor_time, wind_speed):
        if competitor_time == 0:
            __speed = 1
        else:
            __speed = self._speed(competitor_time)
            if __speed > wind_speed:
                __speed -= (self._specs[self.competitor_name]["drag_coef"] * wind_speed)
        return __speed


class Weather:
    """
    Погода
    """
    @staticmethod
    def wind_speed():
        return randint(0, 2)

    @staticmethod
    def air_humidity():
        return randint(30, 60)

    @staticmethod
    def air_temperature():
        return randint(25, 35)


class Competition:
    """
    Соревнование
    """
    _exist_instance = None
    _competitor_time = 0

    def __new__(cls, *args, **kwargs):
        if cls._exist_instance is None:
            cls._exist_instance = super().__new__(cls)
        return cls._exist_instance

    def __init__(self, transport, distance=0):
        self.transport = transport
        self._distance = distance

    def start(self, car, competitors, weather):
        for __competitor_name in competitors:
            car.competitor_name = __competitor_name

            for distance in range(self._distance):
                __wind_speed = weather.wind_speed()
                __speed = car.speed(self._competitor_time, __wind_speed)
                self._competitor_time += float(1) / __speed

            self.end(__competitor_name)

    def end(self, competitor_name):
        print("Car <%s> result: %f" % (competitor_name, self._competitor_time))


print("===Car===")
car = Car(CAR_SPECS)
weather = Weather()
competition = Competition(car, distance=10000)
competitors = ('ferrary', 'bugatti', 'toyota', 'lada', 'sx4')
competition.start(car, competitors, weather)

print()
print("===Motorcycle===")
moto = Motorcycle(MOTO_SPECS)
competition = Competition(car, distance=5000)
competitors = ('ducati', 'bmw')
competition.start(moto, competitors, weather)
